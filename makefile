FC = ifort

# FLGS = -O2 -mieee-fp -align dcommons -convert big_endian -static_intel
# FLAGS = -g -mieee-fp -align dcommons -convert big_endian -static_intel
FLAGS = -g -openmp -mieee-fp -align dcommons -convert big_endian -static_intel

TARGET = camx_regrid

MODULES = \
class_uam_iv.o \
bspline_sub_module.o
PROGRAMS = \
camx_regrid.o

OBJECTS = $(MODULES) $(PROGRAMS)

camx_regrid: $(OBJECTS)
	$(FC) $^ -o $@ $(FLAGS)

%.mod: %.f90
	$(FC) -c $^ $(FLAGS)

%.o: %.f90
	$(FC) -c $^ $(FLAGS)

.PHONY: clean

clean:
	rm -f $(OBJECTS) *.mod