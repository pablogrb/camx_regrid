PROGRAM camx_regrid

USE class_UAM_IV
USE bspline_sub_module

IMPLICIT NONE

!	------------------------------------------------------------------------------------------
!	Purpose:
!		Takes a CAMx area inventory file and writes a new file in a different grid wiht the
!		using the same projection
!	Inputs:
!		Existing CAMx area emissions file
!		Output grid information:
!			Number of cells (nx, ny)
!			Projectin coordinates of the origin (orgx, orgy)
!			Size of cells (dx, dy)
!	Outputs:
!		Regridded emission file
!	By:
!		Pablo Garcia
!		pablogar@andrew.cmu.edu
!		07-2017
!	NOTE:
!		This code requires a Fortran 2003 compatible compiler

!	------------------------------------------------------------------------------------------
!	Declarations

!	UAM-IV file module
	TYPE(UAM_IV) :: fl_inp							! Input emissions file
	TYPE(UAM_IV) :: fl_out							! Output emissions file

!	IO
	CHARACTER(LEN=256) :: ctrlfile					! Control namelist
	CHARACTER(LEN=256) :: fl_inp_name				! File names for IO
	CHARACTER(LEN=256) :: fl_out_name				! File names for IO

!	Grid parameter
	INTEGER :: nx, ny								! Number of cells
	REAL :: utmx, utmy								! Projection coordinates of the origin
	REAL :: dx, dy									! Size of cells

!	Location arrays
	REAL, ALLOCATABLE :: inp_loc_x(:)
	REAL, ALLOCATABLE :: inp_loc_y(:)
	REAL, ALLOCATABLE :: out_loc_x(:)
	REAL, ALLOCATABLE :: out_loc_y(:)

!	Argument control
	INTEGER :: arg_num
	CHARACTER(LEN=2) :: arg_switch

!	Interpolation
	INTEGER kx, ky									! Spline order + 1 (e.g. 3 = cuadratic)
	REAL(8), ALLOCATABLE :: tx(:), ty(:)				! The (nx+kx) knots in the direction for the spline interpolant
	REAL(8), ALLOCATABLE :: bcoef(:,:)					! (nx,ny) matrix of coefficients of the b-spline interpolant
	INTEGER :: iflag								! Calculation status (see the bspline code)

!	Counters
	INTEGER :: i_hr, i_sp, i_nx, i_ny

!	Namelists
	INTEGER :: nml_unit
	NAMELIST /FILE_IO/ fl_inp_name, fl_out_name
	NAMELIST /GRID_DEF/ nx, ny, utmx, utmy, dx, dy

!	------------------------------------------------------------------------------------------
!	Entry point
!	------------------------------------------------------------------------------------------
!
!	Command line argument capture
	arg_num = COMMAND_ARGUMENT_COUNT()
	IF (arg_num .EQ. 0) THEN
		ctrlfile = 'camx_regrid.in'
		WRITE(*,*) 'Using the control file ', TRIM(ctrlfile)
	ELSEIF (arg_num .NE. 2) THEN
		WRITE(*,*) 'Bad argument number'
		CALL EXIT(0)
	ELSE
!		Capture the argument type
		CALL GET_COMMAND_ARGUMENT(1,arg_switch)
		IF (arg_switch .NE. '-f') THEN
			WRITE(*,*) 'Bad argument type'
			CALL EXIT(0)
		ELSE
			CALL GET_COMMAND_ARGUMENT(2,ctrlfile)
			WRITE(*,*) 'Using the control file ', TRIM(ctrlfile)
		END IF	
	END IF

!	Read the namelist
	OPEN(NEWUNIT=nml_unit, FILE=ctrlfile, FORM='FORMATTED', STATUS='OLD', ACTION='READ')
	READ(nml_unit,NML=FILE_IO)
	READ(nml_unit,NML=GRID_DEF)
	CLOSE(nml_unit)

!	------------------------------------------------------------------------------------------
!	Open the input file
	CALL read_uamfile(fl_inp,fl_inp_name)
!	Check for file type
	IF (fl_inp%ftype .NE. 'EMISSIONS') THEN
		WRITE(*,*) 'Not a valid file type'
		CALL EXIT(0)
	END IF

!	Build the location vectors for the input
	ALLOCATE(inp_loc_x(fl_inp%nx))
	DO i_nx = 1,fl_inp%nx
		inp_loc_x(i_nx) = fl_inp%utmx + fl_inp%dx*REAL(i_nx - 1) + fl_inp%dx/2.
	END DO
	ALLOCATE(inp_loc_y(fl_inp%ny))
	DO i_ny = 1,fl_inp%ny
		inp_loc_y(i_ny) = fl_inp%utmy + fl_inp%dy*REAL(i_ny - 1) + fl_inp%dy/2.
	END DO

!	Build the location vectors for the output
	ALLOCATE(out_loc_x(nx))
	DO i_nx = 1,nx
		out_loc_x(i_nx) = utmx + dx*REAL(i_nx - 1) + dx/2.
	END DO
	ALLOCATE(out_loc_y(ny))
	DO i_ny = 1,ny
		out_loc_y(i_ny) = utmy + dy*REAL(i_ny - 1) + dy/2.
	END DO

	! WRITE(*,*) out_loc_x(1), inp_loc_x(1)
	! WRITE(*,*) out_loc_y(1), inp_loc_y(1)
	! WRITE(*,*) out_loc_x(nx), inp_loc_x(fl_inp%nx)
	! WRITE(*,*) out_loc_y(ny), inp_loc_y(fl_inp%ny)

!	Check if the output domain is a subset of the original domain
	IF ( (out_loc_x(1) .LE. inp_loc_x(1)) .OR. &					! SW x
		&(out_loc_y(1) .LE. inp_loc_y(1)) .OR. &					! SW y
		&(out_loc_x(nx) .GE. inp_loc_x(fl_inp%nx)) .OR. &			! NE x
		&(out_loc_y(ny) .GE. inp_loc_y(fl_inp%ny)) ) THEN			! NE y

		WRITE(*,*) 'Output Grid Error'
		WRITE(*,*) 'The output domain is not entirely contain in the original domain'
		CALL EXIT(0)
	END IF

!	------------------------------------------------------------------------------------------
!	Build the output file header
	CALL clone_header(fl_inp, fl_out)
!	Update the header to the new grid
	fl_out%utmx = utmx
	fl_out%utmy = utmy
	fl_out%dx   = dx
	fl_out%dy   = dy
	fl_out%nx   = nx
	fl_out%ny   = ny

!	Clone species list
	fl_out%spname   = fl_inp%spname
	fl_out%c_spname = fl_inp%c_spname

!	------------------------------------------------------------------------------------------
!	Start parallel section
!$OMP PARALLEL PRIVATE(kx, ky, tx, ty, bcoef, iflag)

!	Set the spline order
	kx = 4
	ky = 4
!	Allocate the interpolation parameters
	ALLOCATE(tx(fl_inp%nx+kx), ty(fl_inp%ny+ky))
	ALLOCATE(bcoef(fl_inp%nx, fl_inp%ny))

!	Parallel do loop
!$OMP DO SCHEDULE(DYNAMIC)
!	Loop through species
	DO i_sp = 1,fl_inp%nspec
	! DO i_sp = 1,1
!		Loop over hours
		DO i_hr = 1,fl_inp%update_times
		! DO i_hr = 1,1
!			Calculate the interpolation coefficients
			CALL db2ink(DBLE(inp_loc_x),fl_inp%nx,DBLE(inp_loc_y),fl_inp%ny, &
				&       DBLE(fl_inp%aemis(:,:,i_hr,i_sp)),kx,ky,0,tx,ty, &
				&       bcoef,iflag)
			IF (iflag .NE. 0) THEN
				WRITE(*,*) 'Interpolation parameter calculation failed with code', iflag
				CALL EXIT(0)
			END IF
		END DO
	END DO
!$OMP END DO NOWAIT


!	End of the parallel section
!$OMP END PARALLEL

END PROGRAM camx_regrid
